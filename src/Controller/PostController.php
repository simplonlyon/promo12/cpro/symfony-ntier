<?php

namespace App\Controller;

use App\Business\PostBusinessInterface;
use App\Form\PostType;
use App\Form\UpdatePostType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{

    private PostBusinessInterface $postBusiness;
    public function __construct(PostBusinessInterface $postBusiness) {
        $this->postBusiness = $postBusiness;
    }
    /**
     * @Route("/new-post/{id}", name="new_post")
     */
    public function index(int $id, Request $request )
    {
        $form = $this->createForm(PostType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $this->postBusiness->addPost($post, $id);
            return $this->redirectToRoute('one_topic', ['id' => $id]);
        }
        return $this->render('post/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/update-post/{id}", name="update_post")
     */
    public function update(int $id, Request $request )
    {
        $form = $this->createForm(UpdatePostType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $this->postBusiness->updatePostContent($id, $post->getContent());
            return $this->redirectToRoute('one_topic', ['id' => $id]);

        }
        return $this->render('post/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
