<?php

namespace App\Controller;

use App\Business\TopicBusinessInterface;
use App\Form\PostType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TopicController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(TopicBusinessInterface $topicBusiness, Request $request)
    {
        $page = $request->get('page') ? $request->get('page'):1;

        $pageNumber =  ceil($topicBusiness->getTopicCount() / 25);
        return $this->render('topic/index.html.twig', [
            'topics' => $topicBusiness->getTopics($page),
            'pageNumber' => $pageNumber
        ]);
    }
    /**
     * @Route("/new-topic", name="new_topic")
     */
    public function addTopic(TopicBusinessInterface $topicBusiness, Request $request) {
        $post = null;
        $form = $this->createForm(PostType::class,$post)
        ->add('title', TextType::class, [
            'mapped' => false
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $id = $topicBusiness->addTopic($form->get('title')->getData(), $post);
            return $this->redirectToRoute('one_topic', ['id' => $id]);

        }
        return $this->render('topic/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/topic/{id}", name="one_topic")
     */
    public function oneTopic(int $id, TopicBusinessInterface $topicBusiness) {

        return $this->render('topic/one.html.twig', [
            'topic' => $topicBusiness->getTopic($id)
        ]);
    }
}
